# ESP01 with DS18B20 and Mqtt broker

This project consist out of simple and easy to use Arduino IDE projects for your ESP01 with a DS18B20 and a local MQTT broker


# Features
  - Secrets (WIFI,MQTT settings) and excecutable code 
  - Supports ESP.DeepSleep feature read after completing the requirements

# Including secrets
  - Rename *SecretExample.h to *Secret.h (* is the type of secret mqtt or wifi)
  - Replace the required constants and you are good to go
  
# Requirements
  -   To allow the deepSleep to work you need to solder GPIO16 to the Reset pin of the ESP01
  -   The tutorial can be found on the following page: [DeepSleepTutorial]
  
Disclaimer: 
 If you dont want to use your ESP01 with deepSleep, remove line of code saying ESP.DeepSleep
 If you dont remove it and dont do the soldering your ESP01 will not wake up!

### Tech

Home automation project uses a number of open source projects to work properly:
* [ArduinoIDE] - IDE for developing Ino projects and C++


And of course Home automation itself is open source with a [public repository][gitlab]
 on Gitlab.

### Installation
- Clone the project [git-repo-url]
- Open ArduinoIDE
- Open the desired INO file
- And you are good to go




### Todos

 - Add more modules for multiple diffent sensors




**Free Software, Hell Yeah!**

   [gitlab]: <https://gitlab.com/kzsombik/arduino-homeautomation>
   [git-repo-url]: <https://gitlab.com/kzsombik/arduino-homeautomation.git>
   [ArduinoIDE]: <https://www.arduino.cc/en/main/software>
   [DeepSleepTutorial]: <https://www.instructables.com/id/Enable-DeepSleep-on-an-ESP8266-01/>
   
