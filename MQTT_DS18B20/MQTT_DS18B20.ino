#include <OneWire.h>
#include <DallasTemperature.h>

#include "MqttApi.h"
#include "WifiSecret.h"

#define ONE_WIRE_BUS 2

//This project is made for a DS18B20 sensor and ESP01

char* topic = "iot/temperature/office/tele/SENSOR/TEMP"; //replace the topic you want to use
String temp = "0.0";
const int sleepTimeSeconds = 15;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
WiFiClient espClient;
MqttApi mqttApi(espClient);


void setup() {
  Serial.begin(115200);
  sensors.begin();
  sensors.setResolution(10);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(2000);
  }
}


void temperature() {
  sensors.requestTemperatures();
  if (isnan(sensors.getTempCByIndex(0))) {
    Serial.println(F("Error reading temperature!"));
  } else  {
    mqttApi.publishToTopic(topic, (String) sensors.getTempCByIndex(0));
  }
  

}

void deepSleepESP(){
  ESP.deepSleep(sleepTimeSeconds * 1000000);
}

void loop() {
  mqttApi.reconnectToMqtt();
  temperature();
  deepSleepESP();
  delay(10);
  
}
