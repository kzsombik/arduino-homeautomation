#ifndef TiltSensor_h
#define TiltSensor_h

#include "Arduino.h"

class TiltSensor
{
  public:
  TiltSensor(uint8_t tiltPin);
  TiltSensor(int tiltPin);
  boolean isTilted();
  private:
  uint8_t _tiltPin;
};

#endif
