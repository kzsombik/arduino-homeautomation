#include "Arduino.h"
#include "TiltSensor.h"

TiltSensor::TiltSensor(uint8_t tiltPin)
{
  _tiltPin = tiltPin;
  pinMode(_tiltPin, INPUT);
}

TiltSensor::TiltSensor(int tiltPin)
{
  _tiltPin = tiltPin;
  pinMode(_tiltPin, INPUT);
}

boolean TiltSensor::isTilted()
{
  if (digitalRead(_tiltPin) == HIGH)
  {
    return true;
  }
  return false;
}
