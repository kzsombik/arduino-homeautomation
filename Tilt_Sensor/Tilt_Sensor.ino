#include "TiltSensor.h"
#define cmdPin  3

TiltSensor tiltSensor(cmdPin);

void setup() {
  Serial.begin(115200);
}

void loop() {
  if (tiltSensor.isTilted()) {
    Serial.println("Im tilted");
  }
  else {
    Serial.println("Im not tilted");
  }
}
