#include "Sonar.h"

long emitionDelay = 10;
int echoPin = 7;
int trigerPin = 8;

Sonar sonar (echoPin, trigerPin, emitionDelay);

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print("cm : ");
  Serial.println(sonar.getDistanceInCm());
  Serial.print("inch :");
  Serial.println(sonar.getDistanceInInch());
  delay(100);
}
