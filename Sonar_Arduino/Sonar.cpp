#include "Arduino.h"
#include "Sonar.h"


Sonar::Sonar(int echoPin, int triggerPin, int delayEmit)
{
  _delayEmit = delayEmit;
  _echoPin = echoPin;
  _triggerPin = triggerPin;
  pinMode(_echoPin, INPUT);
  pinMode(_triggerPin, OUTPUT);
}
long Sonar::getDistanceInCm() {
  return emitAndProcess() / convertToCM;
}

long Sonar::getDistanceInInch()
{
  return emitAndProcess() / convertToInch ;
}

long Sonar::emitAndProcess()
{
  digitalWrite(_triggerPin, LOW);
  delayMicroseconds(2);
  digitalWrite(_triggerPin, HIGH);
  delayMicroseconds(_delayEmit);
  digitalWrite(_triggerPin, LOW);
  return pulseIn(_echoPin, HIGH);
}
