#ifndef Sonar_h
#define Sonar_h

class Sonar
{
    const double convertToCM = 58.2; // Speed of sound aprox 28.1 cm/ms x 2 at 20 degrees Celcius
    const double convertToInch = 147.8; // Speed of sound aprox 147.8 inch/ms x 2 at 20 degrees Celcius

  public:
    Sonar(int echoPin, int triggerPin, int delayEmit);
    long getDistanceInCm();
    long getDistanceInInch();

  private:
    long emitAndProcess();
    int _delayEmit;
    uint8_t _echoPin;
    uint8_t _triggerPin;

};

#endif
