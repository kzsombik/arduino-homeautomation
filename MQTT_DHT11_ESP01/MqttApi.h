#ifndef MqttApi_h
#define MqttApi_h

#include "Arduino.h"
#include <PubSubClient.h>
#include <ESP8266WiFi.h>


class MqttApi
{
  public:
    MqttApi();
    MqttApi(WiFiClient espClient);
    void publishToTopic(char* topic, String dataTosend);
    void reconnectToMqtt();
  private:
    PubSubClient _mqttClient;

};


#endif
