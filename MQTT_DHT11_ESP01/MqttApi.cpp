#include "Arduino.h"
#include "MqttApi.h"
#include "MqttSecret.h"

#include <PubSubClient.h>
MqttApi::MqttApi(WiFiClient espClient)
{
  _mqttClient = PubSubClient(espClient);
}

void MqttApi::publishToTopic(char* topic, String stringData) {
  char dataToSend[stringData.length() + 1];
  stringData.toCharArray(dataToSend, stringData.length() + 1);
  _mqttClient.publish(topic, dataToSend);
  delay(150);
}

void MqttApi::reconnectToMqtt() {
  _mqttClient.setServer(mqttServer, mqttPort);
  while (!_mqttClient.connected()) {
    if (_mqttClient.connect(hostMqttClient, mqttUser, mqttPassword )) {
      Serial.println("connected");
    } else {
      Serial.println(_mqttClient.state());
      delay(5000);
    }
  }
}
