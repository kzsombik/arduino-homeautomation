#include <DHT_U.h>
#include "MqttApi.h"
#include "WifiSecret.h"

#define DHTTYPE DHT11
#define DHTPIN 2

char* topicTemp = "iot/temperature/tele/SENSOR/TEMP";
char* topicHumidity = "iot/temperature/tele/SENSOR/HUMIDITY";
float correctionDHT11 = 2.0;

DHT_Unified dht(DHTPIN, DHTTYPE);
sensor_t sensor;

WiFiClient espClient;
MqttApi mqttApi(espClient);
uint32_t delayMS;


void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  dht.begin();
  dht.temperature().getSensor(&sensor);
  delayMS = sensor.min_delay / 50;
  while (WiFi.status() != WL_CONNECTED) {
    delay(2000);
  }
}

void temperature(sensors_event_t event) {
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
  } else
  {
    float temperature = event.temperature - correctionDHT11;
    mqttApi.publishToTopic(topicTemp, (String) temperature);
  }
}

void humidity(sensors_event_t event) {
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading humidity!"));
  }
  else {
    mqttApi.publishToTopic(topicHumidity, (String)event.relative_humidity);
  }
}

void loop() {
  mqttApi.reconnectToMqtt();
  sensors_event_t event;
  temperature(event);
  humidity(event);
  delay(delayMS);
}
