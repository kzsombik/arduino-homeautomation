# Home automation with ESP01 and Mqtt broker


This project consist out of simple and easy to use Arduino IDE projects
Currently this project has following examples ready for use:
  - ESP01 with a DHT11 and a local MQTT broker
  - ESP01 with a DS18B20 and a local MQTT broker
  - Arduino with a HC-SR04 sensor 
  - Arduino with a Reed sensor
  - Arduino with a Tilt sensor


# Features

 ##### Only available for ESP01 Projects:
  - Secrets (WIFI,MQTT settings) and excecutable 

# Including secrets
  - Rename *SecretExample.h to *Secret.h (* is the type of secret mqtt or wifi)
  - Replace the required constants and you are good to go
  

### Tech

Home automation project uses a number of open source projects to work properly:
* [ArduinoIDE] - IDE for developing Ino projects and C++


And of course Home automation itself is open source with a [public repository][gitlab]
 on Gitlab.

### Installation
- Clone the project [git-repo-url]
- Open ArduinoIDE
- Open the desired INO file
- And you are good to go




### Todos

 - Add readme for each sub module
 - Add more modules for multiple diffent sensors




**Free Software, Hell Yeah!**

   [gitlab]: <https://gitlab.com/kzsombik/arduino-homeautomation>
   [git-repo-url]: <https://gitlab.com/kzsombik/arduino-homeautomation.git>
   [ArduinoIDE]: <https://www.arduino.cc/en/main/software>
   
