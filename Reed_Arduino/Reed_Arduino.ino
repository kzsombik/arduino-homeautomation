#include "ReedSensor.h"

#define digitalPin  7 // KY-025 digital interface
#define analogPin  A0 // KY-025 analog interface

ReedSensor reedSensor(digitalPin, analogPin);

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  if (reedSensor.magnetDetected()) // if magnetic field is detected
  {
    Serial.println("Magnet detected");
    Serial.println("Analog value: " + reedSensor.readAnalogValue());
  }
  else
  {
    Serial.println("No magnet detected");
    Serial.println("Analog value: " + reedSensor.readAnalogValue());
  }
}
