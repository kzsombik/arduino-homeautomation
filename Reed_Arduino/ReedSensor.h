#ifndef ReedSensor_h
#define ReedSensor_h

#include "Arduino.h"

class ReedSensor
{
  public:
    ReedSensor(int digitalPin);
    ReedSensor(int digitalPin, int analogPin);
    boolean magnetDetected();
    int readAnalogValue();
  private:
    uint8_t _digitalPin;
    uint8_t _analogPin;
    boolean _analogAvailable;
};

#endif
