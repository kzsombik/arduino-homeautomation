#include "Arduino.h"
#include "ReedSensor.h"


ReedSensor::ReedSensor(int digitalPin)
{
  _digitalPin = digitalPin;
  _analogPin = 0;
  _analogAvailable = false;
  pinMode(_digitalPin, INPUT);
}

ReedSensor::ReedSensor(int digitalPin, int analogPin) {
  _digitalPin = digitalPin;
  _analogPin = analogPin;
  _analogAvailable = true;
  pinMode(_digitalPin, INPUT);
}

boolean ReedSensor::magnetDetected()
{
  if (digitalRead(_digitalPin) == HIGH)
  {
    return true;
  }
  return false;
}

int ReedSensor::readAnalogValue()
{
  if (_analogAvailable)
  {
    return analogRead(_analogPin);
  }
  return 0;
}
